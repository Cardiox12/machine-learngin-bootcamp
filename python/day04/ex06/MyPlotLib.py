# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    MyPlotLib.py                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/21 23:20:49 by tony              #+#    #+#              #
#    Updated: 2020/03/23 20:45:15 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from FileLoader import FileLoader
import pandas as pd
import matplotlib.pyplot as plt

class MyPlotLib(object):
    def __init__(self):
        pass
    
    def histogram(self, data, features):
        """
            • histogram(data, features) : plots one histogram for each numerical feature in the list
        """

        if isinstance(features, str):
            fig, ax = plt.subplots()
            name = features
            features = data[features].value_counts().to_dict()
            x = list(features.keys())
            y = list(features.values())
            ax.bar(x, y)
            ax.set_title(name)
        elif isinstance(features, list):
            fig, axes = plt.subplots(1, len(features))
            
            for ax, feature in zip(axes, features):
                name = feature
                feature = data[feature].value_counts().to_dict()
                x = list(feature.keys())
                y = list(feature.values())
                ax.bar(x, y)
                ax.set_title(name)
        else:
            raise TypeError("Argument features should be a list or str.")
            
    
    def density(self, data, features):
        if isinstance(features, str):
            data[features].plot(kind="density", legend=[features])
        elif isinstance(features, list):
            for feature in features:
                feature = data[feature]
                feature.plot(kind="density", legend=features)
        else:
            raise TypeError("Argument features should be a list or str.")
    
    def pair_plot(self, data, features):
        pass
    
    def box_plot(self, data, features):
        pass

data = FileLoader().load("ressources/datasets/athletes_events.csv")
data = data.head(100)
mplt = MyPlotLib()

plt.show()