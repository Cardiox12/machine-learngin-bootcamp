# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    YoungerFellah.py                                   :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/20 00:22:31 by tony              #+#    #+#              #
#    Updated: 2020/03/20 00:50:24 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from FileLoader import FileLoader
import pandas as pd

def youngestFellah(df, year):
    years = df.loc[df["Year"] == year]
    
    male = years.loc[years["Sex"] == "M"]
    female = years.loc[years["Sex"] == "F"]

    return (
        {
            "male" : male["Age"].min(),
            "female" : female["Age"].min()
        }
    )