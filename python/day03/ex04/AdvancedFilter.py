# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    AdvancedFilter.py                                  :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/18 22:32:42 by tony              #+#    #+#              #
#    Updated: 2020/03/19 23:53:33 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import math
import time

class ImageProcessor(object):
	def __init__(self):
		pass

	def load(self, path):
		img = mpimg.imread(path)
		height, width, _ = img.shape
		print(f"Loading image of dimensions {width}x{height}")
		return img

	def display(self, array):
		plt.imshow(array)
		plt.show()
    
class AdvancedFilter(object):
    def __init__(self):
        pass

    def pixel_to_float(self, pixel):
        return (pixel * 100) // 255 / 100

    def mean_blur(self, array, ksize=4):
        """
            This method receives an image, performs a mean blur on it and returns
            a blurred copy. 
            In a mean blur, each pixel becomes the average of its neighboring pixels. 
        """
        new = np.copy(array)
        ksize += 1

        for y in range(array.shape[0]):
            for x in range(array.shape[1]):
                sub_r = array[y:y + ksize,x:x + ksize,0:]
                sub_g = array[y:y + ksize,x:x + ksize,1:]
                sub_b = array[y:y + ksize,x:x + ksize,2:]

                new[y,x,0] = np.sum(sub_r) / sub_r.size
                new[y,x,1] = np.sum(sub_g) / sub_g.size
                new[y,x,2] = np.sum(sub_b) / sub_b.size
        
        return new

    def _gen_gauss_kernel(self, ksize, value):
        """ 
            This method returns a gaussian kernel.
        """
        if ksize < 0 or value < 0:
            raise ValueError(f"{'Kernel size' if ksize < 0 else 'Value'} cannot be negative.")
        if ksize > value:
            raise ValueError("Kernel size should be greater or equal than the middle value.")
        if ksize % 2 == 0:
            ksize += 1
        kernel = np.zeros((ksize, ksize), dtype="int")

        midx = kernel.shape[1] // 2
        midy = kernel.shape[0] // 2

        for x in range(kernel.shape[1]):
            for y in range(kernel.shape[0]):
                dist = math.sqrt(
                    (midx - x) ** 2 + (midy - y) ** 2
                )
                kernel[y, x] = value - dist

        return kernel
        
    def gaussian_blur(self, array, ksize=6):
        """ 
            This method receives an image, performs a gaussian blur on it and returns
            a blurred copy. 
            In a gaussian blur, the weighting of the neighboring pixels is adjusted
            so that closer pixels are more heavily counted in the average.

            Algorithm :
                - apply
        """
        new = np.copy(array)
        kernel = self._gen_gauss_kernel(ksize, ksize)
        ksize += 1

        for y in range(array.shape[0]):
            for x in range(array.shape[1]):
                sub_r = array[y:y + ksize,x:x + ksize,0]
                sub_g = array[y:y + ksize,x:x + ksize,1]
                sub_b = array[y:y + ksize,x:x + ksize,2]
                
                sub_r = np.matmul(sub_r, kernel[:sub_r.shape[0],:sub_r.shape[1]])
                sub_g = np.matmul(sub_g, kernel[:sub_g.shape[0],:sub_g.shape[1]])
                sub_b = np.matmul(sub_b, kernel[:sub_b.shape[0],:sub_b.shape[1]])

                # sub_r = sub_r * kernel[:sub_r.shape[0],:sub_r.shape[1]]
                # sub_g = sub_g * kernel[:sub_g.shape[0],:sub_g.shape[1]]
                # sub_b = sub_b * kernel[:sub_b.shape[0],:sub_b.shape[1]]

                new[y,x,0] = np.sum(sub_r) / sub_r.size
                new[y,x,1] = np.sum(sub_g) / sub_g.size
                new[y,x,2] = np.sum(sub_b) / sub_b.size
                
        return new