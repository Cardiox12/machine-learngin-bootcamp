# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ScrapBooker.py                                     :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/16 15:55:14 by bbellavi          #+#    #+#              #
#    Updated: 2020/03/17 18:57:02 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import numpy as np

VERTICAL = 0
HORIZONTAL = 1

class ScrapBooker(object):
	def __init__(self):
		pass

	def crop(self, array, dimensions, position=(0, 0)):
		"""
			Crop an image at position and dimensions.
		"""
		return array[
			position[0]:position[0] + dimensions[0],
			position[1]:position[1] + dimensions[1]
		]

	def thin(self, array, n, axis):
		"""
			delete every n-th pixel row along the specified axis (0 vertical, 1 horizontal),
			example below.
		"""
		if axis == VERTICAL:
			return np.delete(array, np.arange(n - 1, array[0].size, n), axis=1)
		if axis == HORIZONTAL:
			return np.delete(array, np.arange(n - 1, array.size, n), axis=0)

	def juxtapose(self, array, n, axis):
		"""
			Juxtapose (concat) n copies of the image along the specified axis (0 vertical, 1 horizontal).
			Similar to concatenate function of numpy module.
		"""

		if axis == VERTICAL:
			return np.tile(array, (n, 1))
		if axis == HORIZONTAL:
			return np.tile(array, (1, n))

	def mosaic(self, array, dimensions):
		return np.tile(array, dimensions)